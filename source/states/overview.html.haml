---
title: States / Overview - Webface.js
---

- @page_id = "states/overview"
.title
  %h1 States / Overview

.content

  %p
    States are Webface's implementation of a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">state machine</a>.
    It allows you to write declarative statements in your component class and invoke certain code or show/hide
    certain component parts and child-components when component changes its state.

  %p.note
    Check states usage example and experiment with code using the
    <a href="/examples/services/states">example page for states</a>.

  %h2 Basic mechanism of work

  %p
    Component state is defined by ALL of its attributes and their values at any given moment. Let's say we have a
    <code class="inline language-javascript">UserComponent</code> with three attributes:
    <code class="inline language-javascript">email</code>, <code class="inline language-javascript">password</code> and
    <code class="inline language-javascript">country</code>. Whenever any of those attributes is changed with either
    <code class="inline language-javascript">UserComponent.set()</code> or
    <code class="inline language-javascript">UserComponent.updateAttributes()</code> the component checks two things:

    %ol
      %li Which state the component left
      %li Which new state the component has entered

    Based on this, it will invoke the specified code or show/hide certain parts and child-components.
    The code that's being run for those two cases is called a <b><i>transition</i></b>. You can have an <b><i>in</i></b> transition
    (code to invoked on entering a certain state) and an <b><i>out</i></b> transition (code invoked when leaving the state).

  %p.note.important
    %b.important IMPORTANT!
    This is a bit different from the common definition of a state machine. Normally, in a state machine <i>a transition</i> is
    something that actually changes the state after receiving a so called <i>signal</i>. There is no signal in Webface.js - whenever
    attributes are updated, the component checks whether any of the state declarations match the new state
    and then decides what to do about based on the declarative instructions given - the act of doing that is called
    <i>the transition</i> in Webface.js terminology. This makes a lot more sense in UI-context.
    DO NOT CONFUSE how it works. Once again, in Webface.js <b>change of attribute values (state) causes transitions
    to be invoked</b>, not the other way round. You can think of it this way: a component tells you
    "Hey, I have this much money now and I'm N years old" and you help it pick the best outfit and give financial advisors
    phone - that is, you help it do things that fit its circumstances.

  %h2 Defining states and transitions

  %p
    Webface.js components process states with instances of the <code class="inline language-javascript">StateManager</code> class,
    of which there are two descendants: <code class="inline language-javascript">StateActionManager</code> and
    <code class="inline language-javascript">DisplayStateManager</code>.
    <code class="inline language-javascript">StateDispatcher</code> does the groundwork of detecting certain attributes changed
    and routing requests to these manager instances. You don't have to look inside those classes or even know those
    classes exist, but it's good to know what they are to understand things better.

  %p.note
    Notice a bit of an inconsistency in naming the classes: the word <i>State</i> appears before the word <i>Action</i> in
    <code class="inline language-javascript">StateActionManager</code> and after the word <i>Display</i> in
    <code class="inline language-javascript">DisplayStateManager</code> - that's because in English it reads better that way,
    but you probably want to name <code class="inline language-javascript">StateManager</code> descendants so that
    the word <i>State</i> goes after the first word that indicates the purpose of that state manager class
    (unlikely you need to code your own manager class though!).

  %p
    To define a state and a transition for it, you define a property inside your component class' constructor:

    %pre.line-numbers
      %code.language-javascript
        :erb
          class UserComponent extends extend_as("UserComponent").mix(Component).with() {
            constructor() {
              this.attribute_names = ["country", "age", "occupation"];
              this.states = [
                ...
              ];
            }
          }

    <code class="inline language-javascript">this.states</code> is an array where almost each element is a
    <i>state declaration</i> - however there are exceptions. We also need to let the
    <code class="inline language-javascript">StateDispatcher</code> know which
    <code class="inline language-javascript">StateManager</code> instance to use. So, to improve
    the readability of state declarations array, instead of using nested arrays and objects, we simply
    have to make sure that we specify state manager name and options before we list all state declarations. So, in essence,
    <code class="inline language-javascript">this.states</code> becomes a flat array that looks like this:

    %pre.line-numbers
      %code.language-javascript
        :erb
          this.states = [
            "action", { pick_states_with_longest_definition_only: false }, // <-- state manager name and options
            [{ country: "Dictatorstan" }, "blockByCountry"],               // <-- state declaration
            [{ age: { less_than: 17 }  }, "blockByAge"]                    // <-- state declaration
          ];

  %p
    The declarations above will invoke <code class="inline language-javascript">UserComponent.blockByCountry()</code>
    and <code class="inline language-javascript">UserComponent.blockByAge()</code> functions when attribute values
    change to the specified values. Only <i>in</i> transitions are specified in these two declrations - we'll look at how
    to specify <i>out</i> transitions later.


  %h2 Terminology

  %p
    Before we move on, let's go over the correct terminilogy for state declarations and what's inside it. Here's
    an annotated state declaration:

    %pre.line-numbers
      %code.language-javascript
        :erb
          /* declaration -->*/ [  { attr1: "value1", attr2: "value2" }, "some_important_button"  ]
          //                   |  |-----------------^----------------|  |----------^----------|  |
          //                   |            state definition                state transition     |
          //                   |------------------------------^----------------------------------|
          //                                            state declaration

    Let's go over the more precise descriptions of each term:

    %ul
      %li
        <b><i>state definition</i></b> - a set of attributes and their values that defines a state for the component.
        It's how we know a component is in a certain state by checking those attributes and values
        and seeing that their values match the ones in the definition. For example, this is a state definition:
        <code class="inline language-javascript">{ attr1: "value1", attr2: ["value2", "value3"] }</code>
        It says that in order for the transitions (listed next) to be invoked, the component
        needs to have its <code class="inline language-javascript">attr1</code> value be equal to
        <code class="inline language-javascript">"value1"</code> AND its <code class="inline language-javascript">attr2</code>
        value be equal to either <code class="inline language-javascript">"value2"</code> or
        <code class="inline language-javascript">"value3"</code>.

      %li
        <b><i>state transition</i></b> - this can be whatever it needs to be. Its use is defined by classes that extend
        <code class="inline language-javascript">StateManager</code>. In case of
        <code class="inline language-javascript">DisplayStateManager</code> it can
        be an array of entity names (component parts and children roles, see <a href="display-state-manager">DisplayStateManager</a> page)
        that need to be hidden or shown, but for <code class="inline language-javascript">StateActionManager</code> it's
        just the function names defined in your component that need to be invoked.

      %li
        <b><i>state declaration</i></b> is
        <code class="inline language-javascript">state definition + state transition + folded states</code>
        (<a href="folded-states">folded states</a> to be explained further).


  = partial "shared/nav", locals: { prev: "Events/Event locks", nxt: "States/State definitions" }
