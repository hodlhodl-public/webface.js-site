---
title: States / Transition ordering - Webface.js
---

- @page_id = "states/transition-ordering"
.title
  %h1 States / Transition ordering

.content

  %p
    Sometimes, to ensure your UI behaves correctly, it is important to know, understand and adjust
    the order of transition invocation for a given state. There are two types of transition ordering we can talk about:

    %ol
      %li The order in which different state managers apply their sets of transitions
      %li The order in which transitions are applied within the boundaries of one state manager

  %h2 Transition set ordering between state managers

  %p
    The default behavior is for each state manager's set of transitions to be applied
    in the order in which declarations for those state managers are found in the
    <code class="inline language-javascript">this.states</code> array.
    If <code class="inline language-javascript">StateActionManager</code> declarations are added into that array
    before <code class="inline language-javascript">DisplayStateManager</code> declarations then they will be applied first.

  %p
    However, sometimes you want to bypass that order for certain - but not all - transitions of a given state manager.
    A real world example would be generating a QR code. The QR code library in the example needs a visible dom element
    to correctly generate the QR code image. If the dom element is hidden, it will not do it. Consider this code:

    %pre.line-numbers
      %code.language-javascript
        :erb
          class OrderComponent extends extend_as("OrderComponent").mix(Component).with() {

            constructor() {

              this.states = [
                "action", {},
                [{ status: "invoiced" }, "generateQRCode"],
                "display", {},
                [{ status: "invoiced" }, "invoice_qr"]
              ];

            }

            generateQRCode() {
              new QRCode({ el: this.findPart("invoice_qr") });
            }
          }

    Because <code class="inline language-javascript">StateActionManager</code> declarations go first it will invoke the
    <code class="inline language-javascript">generateQRCode()</code> method first and only then show the component part
    this QR-code is supposed to appear in. The problem can be easily fixed by placing
    <code class="inline language-javascript">DisplayStateManager</code> declarations above
    <code class="inline language-javascript">StateActionManager</code> declarations, but what if we had other declarations
    for <code class="inline language-javascript">StateActionManager</code> most of which needed their transitions to be applied
    specifically <i>before</i> any of the transitions from <code class="inline language-javascript">DisplayStateManager</code>?
    In that case, we can add a special instruction only to this particular declaration.
    The <code class="inline language-javascript">run_before</code> instruction would delay the application of transitions
    for this one declaration until all transitions from <code class="inline language-javascript">DisplayStateManager</code> are applied:

    %pre.line-numbers
      %code.language-javascript
        :erb
            this.states = [
              "action", {},
              [{ status: "invoiced" }, { in: "generateQRCode", run_after: "display" }],
              "display", {},
              [{ status: "invoiced" }, "invoice_qr"]
            ];

    Or, alternatively, we could've added the <code class="inline language-javascript">run_before</code>
    option to the transition object in <code class="inline language-javascript">DisplayStateManager</code>
    declaration:

    %pre.line-numbers
      %code.language-javascript
        :erb
            this.states = [
              "action", {},
              [{ status: "invoiced" }, "generateQRCode"],
              "display", {},
              [{ status: "invoiced" }, { in: "invoice_qr", run_before: "action" }]
            ];

  %p
    Be careful using many <code class="inline language-javascript">run_before</code> and
    <code class="inline language-javascript">run_after</code> instructions - there's a sorting mechanism
    that would do up to 100 iterations, but if after those 100 iterations it can't figure out
    which transitions should be applied first, it would stop trying to sort and throw. For instance,
    if we wrote the following declarations, an error would be thrown:

    %pre.line-numbers
      %code.language-javascript
        :erb
            this.states = [
              "action", {},
              [{ status: "invoiced" }, { in: "generateQRCode", run_before "display" }],
              "display", {},
              [{ status: "invoiced" }, { in: "invoice_qr", run_before: "action" }]
            ];

    That's because the two <code class="inline language-javascript">run_before</code> instructions are contradicting each other.
    Therefore, the general advice is to use <code class="inline language-javascript">run_before</code> and
    <code class="inline language-javascript">run_after</code> instructions as rare as possible.


  %h2 Transition ordering inside state managers
  %p
    Each state manager decides on its own on the order in which to run transitions.

    %ul
      %li
        <code class="inline language-javascript">StateActionManager</code> will run all transitions
        in the order in which the corresponding state declarations were placed into the
        <code class="inline language-javascript">this.states</code> array, however it will run
        <i>out</i> transitions first, followed by the <i>in</i> transitions. Transitions are run
        synchronously, one after another, (unless the method you're invoking is itself asynchronous).

      %li
        <code class="inline language-javascript">DisplayStateManager</code> has a slightly different approach
        where it would apply transitions, both <i>in</i> and <i>out</i> all at once asynchronously.
        It makes sense because of the nature of DOM-animation: you wouldn't want to wait while each show/hide
        animation is applied one-by-one to each entity - you just want to change what the user sees on the screen
        all at once!

      %li
        Another thing to know about <code class="inline language-javascript">DisplayStateManager</code> is that
        it doesn't queue transitions from different states that the component happened to go through
        while transitions from the first state change were still running - it simply applies a set of transitions
        for the latest state and throws away all of the other ones. This might be a bit confusing, but it also
        makes sense: suppose the animation to show and hide all necessary entities for any given state lasts 1 second.
        During that second a lot can change and your component may actually go through 2 or 3 other state changes. If
        <code class="inline language-javascript">DisplayStateManager</code> were to apply all of them,
        not only would it take additional 2-3 seconds, but all the flashing would surely annoy the user.


  = partial "shared/nav", locals: { prev: "States/Common options", nxt: "Behaviors" }
