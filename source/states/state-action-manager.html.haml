---
title: States / StateActionManager - Webface.js
---

- @page_id = "states/state-action-manager"
.title
  %h1 States / StateActionManager

.content

  %p
    As was mentioned on the <a href="overview.html">overview of states</a> page, there are two types of state managers
    that detect states and run transitions for the matched states. While <code class="inline language-javascript">DisplayStateManager</code>
    is the one you'll end up using the most, we'll start with the more generic <code class="inline language-javascript">StateActionManger</code>.
    If you ever end up writing your own state manager, this is the one you're probably going to inherit from or at least look at.

  %p
    All state declarations that are processed
    by <code class="inline language-javascript">StateActionManager</code> are prepended by the two array elements -
    <code class="inline language-javascript">"action", { ...options }</code> - which indicate that what follows next in the
    <code class="inline language-javascript">this.states</code> array are state declarations to be processed by
    <code class="inline language-javascript">StateActionManager</code>.

  %p
    As the name of this state manager suggests, it simply invokes an <b><i>action</i></b> whenever a certain state is entered
    or exited. <i>Action</i> is simply a method defined in the same component class. Method name for the <i>action</i>
    is passed as a string as the second argument of the state declaration array. However, action can also be a function
    to be called in place. For example, let's write the following code to alert our users that they're below drinking age
    using the first approach (passing method name as a string):

  %pre.line-numbers(data-line="7,11-13")
    %code.language-javascript
      :erb
        class UserComponent extends extend_as("UserComponent").mix(Component).with() {

          constructor() {
            this.attributes = ["age"];
            this.states = [
              "action", {}, // <-- everything that follows after this line will be processed by StateActionManager
              [{ age: { less_than: 18 }}, "alertBelowDrinkingAge"]
            ];
          }

          alertBelowDrinkingAge() {
            alert("You're below the legal drinking age, you can't order alcoholic beverages!");
          }

        }

  %p
    Here, the specified method is an <i>in</i> transition by default. That is, we could've written it as
    <code class="inline language-javascript">[{ age: { less_than: 18 }}, { in: "alertBelowDrinkingAge" }]</code>,
    but since it is assumed that most of the time it's the <i>in</i> transitions that we're going to use,
    it made sense to simplify the syntax a little bit. But the same thing can be rewritten so that we use an in-place
    function which saves us creating a separate method:

  %pre.line-numbers(data-line="8")
    %code.language-javascript
      :erb
        class UserComponent extends extend_as("UserComponent").mix(Component).with() {

          constructor() {
            this.attributes = ["age"];
            this.states = [
              "action", {},
              [{ age: { less_than: 18 }}, {
                in: () => alert(
                 "You're below the legal drinking age," +
                 "you can't order alcoholic beverages!")
              }]
            ];
          }

        }

  .note.important
    %b.important IMPORTANT!
    We used an object for transitions in the second version of the code, which has they key
    <code class="inline language-javascript">in</code> with the value of that key being the function to be invoked.
    In the current version of states implementation, you HAVE to use it like this - simply writing placing a function
    where the name of the actions where supposed to be wouldn't work. That is, this code won't invoke the function:

    %pre.line-numbers(data-line="3")
      %code.language-javascript
        :erb
          this.states = [
            "action", {},
            [{ age: { less_than: 18 }}, () => alert("...")] // <-- WILL NOT WORK!
          ];

  %p
    With the <i>out</i> transitions it's pretty much the same, you just have to specify out transitions for the particular
    state definition:

  %pre.line-numbers(data-line="4,5")
    %code.language-javascript
      :erb
        this.states = [
          "action", {},
          [{ age: { less_than: 18 }}, {
            in:  () => alert("You're below the legal drinking age, you can't order alcoholic beverages!"),
            out: () => alert("Congrats, you're now over the legal drinking age and can order alcoholic beverages!"),
          }]
        ];

  %p
    Each <i>in</i> and <i>out</i> transition can consist of several actions passed as an array, for example:

  %pre.line-numbers(data-line="4,5")
    %code.language-javascript
      :erb
        this.states = [
          "action", {},
          [{ age: { less_than: 18 }}, {
            in:  [() => console.log("..."), () => alert("...")],
            out: "alertOverDrinkingAgeToConsole,alertOverDrinkingAgeAsModalWindow"
          }]
        ];

  %p
    For the <i>in</i> transition we used an array with two functions as its elements, but
    for the <i>out</i> transition we passed method names as one string and separated them with a comma within that string.
    That's possible because <code class="inline language-javascript">StateManager</code> core class functionality
    takes care of that and treats strings with commas in them as something it needs to split into an array of strings.
    We could've written it as
    <code class="inline language-javascript">out: ["alertOverDrinkingAgeToConsole", "alertOverDrinkingAgeAsModalWindow"]</code>
    to much the same effect.

  = partial "shared/nav", locals: { prev: "States/Nested states", nxt: "States/DisplayStateManager" }
