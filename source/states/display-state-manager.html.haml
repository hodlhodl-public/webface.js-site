---
title: States / DisplayStateManager - Webface.js
---

- @page_id = "states/display-state-manager"
.title
  %h1 States / DisplayStateManager

.content

  %p
    Arguably, <code class="inline language-javascript">DisplayStateManager</code> is the state manager
    that will be most useful when you write components. It allows you to declare which elements are visible
    and which elements are hidden on the page based on the current state of the component those states are defined in.
    All display state declarations inside <code class="inline language-javascript">this.state</code> array
    must be preceeded by the two elements - <code class="inline language-javascript">"display", { ...options }</code>.

  %p
    <code class="inline language-javascript">DisplayStateManager</code> works slightly different - in principle -
    from <code class="inline language-javascript">StateActionManager</code>. There are three main things to remember:

    %ol
      %li
        <b>All transitions are <i>in</i> transitions</b> and the default action is to either show or hide the listed entities.
        <code class="inline language-javascript">show()</code>, <code class="inline language-javascript">hide()</code>,
        <code class="inline language-javascript">showPart()</code> and<code class="inline language-javascript">hidePart()</code>
        <a href="/behaviors.html">behaviors</a> specific to your component (if they differ) or to children components are used to perform
        these transitions. If you don't specify it explicitly, the default <i>in</i> transition will be to <i>show</i>
        the listed entities.
      %li
        <b>The <i>out</i> transition to any state is the opposite of the default <i>in</i> transition</b> and it's applied
        to all entities that were not matched to the newly entered state. For instance,
        if the default <i>in</i> transition is to <i>show</i> entities then we simply hide all other entities
        except for the ones that are supposed to be shown for the current state.
      %li
        Any entity that's not listed at all is considered to be exempt from transitions and its visibility
        is defined by either stylesheets or in some other parts of the code unrelated to
        <code class="inline language-javascript">DisplayStateManager</code>. In other words, if it's not listed,
        it's not <code class="inline language-javascript">DisplayStateManager</code>'s job to manage its visibility.

    %p
      <b><i>Entity</i></b> is a term used here to refer to either component part or component child (another component) with
      a specific role. Both types of entities can be easily managed by
      <code class="inline language-javascript">DisplayStateManager</code>. You list those entity names in the
      transition (second) element of the state declaration array:

      %pre.line-numbers
        %code.language-javascript
          :erb
            this.states = [
              "display", {}, // <-- everything that follows after this line will be processed by DisplayStateManager
              [{ first_sign_in: true }, "accept_cookies,tos,tour_link"]
              //                        |--------------^--------------|
              //                                entities list
            ];

      In this example, we assume user has an attribute called <code class="inline language-javascript">first_sign_in</code>
      which is somehow set to true only when it's the first time user signs in. And we want to show this user a few things
      before they can proceed: first - the most annoyingly stupid "This website uses cookies" notification, second - we want to
      ask the user to agree to Terms Of Use by showing them an entity that contains the AGREE button. And, finally,
      we also want to show the user a link to the tour of the website features.

    %p
      The entities listed -
      <code class="inline language-javascript">"accept_cookies", "tos", "tour_link"</code> - can be either component parts or child components
      of our <code class="inline language-javascript">UserComponent</code> instance. The only important thing here is that
      they are indeed contained inside <code class="inline language-javascript">UserComponent</code>'s dom element.
      That's what our html code may look like:

      %script(type="text/plain" class="language-markup line-numbers")
        :plain
          <div data-component-class="UserComponent">
            ...
            <a href="/tour" data-component-part="tour_link">Take a tour of our website's features!</a>
            <button data-component-class="ButtonComponent" data-component-roles="accept_cookies">
              Accept this site uses cookies</button>
            <div data-component-part="tos">Long TOS...</div>
            <button data-component-class="ButtonComponent" data-component-roles="tos">Accept TOS</button>
          </div>

      Here's the cool part: we have two entities that are called <code class="inline language-javascript">tos</code> -
      one of them is <code class="inline language-javascript">UserComponent</code>'s part and the other one
      is a child component with the role of the same name. Both of them are going to be displayed!
      However, if you only wanted to show the component part entity, but not the child component, you'd prepend entity name
      with a dot:

      %pre.line-numbers
        %code.language-javascript
          :erb
            [{ first_sign_in: true }, "accept_cookies,.tos,tour_link"]

      Similarly, if you wanted to show the child component with the role <code class="inline language-javascript">tos</code>, but
      not the component part, you'd prepend the entity name with <code class="inline language-javascript">#</code> like this:

      %pre.line-numbers
        %code.language-javascript
          :erb
            [{ first_sign_in: true }, "accept_cookies,#tos,tour_link"]

      Finally, as mentioned previously, Webface's state manager takes care of strings with commas in them and converts them
      to arrays, so the following two state declarations are identical:

      %pre.line-numbers
        %code.language-javascript
          :erb
            [{ first_sign_in: true }, "accept_cookies,tos,tour_link"]
            // is the same as
            [{ first_sign_in: true }, ["accept_cookies", "tos", "tour_link"]]

    %h2 Options

    %p
      You can pass several options to <code class="inline language-javascript">DisplayStateManager</code>
      that are unique to it:

      %ul
        %li
          <code class="inline language-javascript">default_state_action</code> - can be either
          <code class="inline language-javascript">"show"</code> or <code class="inline language-javascript">"hide"</code>.
          The default value is <code class="inline language-javascript">"show"</code>. This option defines what the <i>in</i>
          transition will be and, consequently, what the <i>out</i> transition is going to be too - for all of state declarations
          processed by <code class="inline language-javascript">DisplayStateManager</code>
          (this was discussed in more detail above).

        %li
          <code class="inline language-javascript">hide_animation_speed</code> and
          <code class="inline language-javascript">show_animation_speed</code> - default is
          <code class="inline language-javascript">500</code> (ms) for both.
          <code class="inline language-javascript">DisplayStateManager</code> would hide elements using
          the <code class="inline language-javascript">hide()/show()/hidePart()/showPart()</code> behaviors
          with the animation length defined by these options and passed along to those behavior methods.
          If set to 0, then no animation is used.

      Here's an example of passing options to <code class="inline language-javascript">DisplayStateManager</code>:

      %pre.line-numbers(data-line="3")
        %code.language-javascript
          :erb
            this.states = [
              "display", { default_state_action: "hide", hide_animation_speed: 0, show_animation_speed: 200 },
              [{ first_sign_in: false }, "accept_cookies,tos,tour_link"]
            ];

      Note that we set <code class="inline language-javascript">default_state_action</code> to
      <code class="inline language-javascript">"hide"</code>, which means that by default every entity that's listed
      for any state is going to be displayed. That's why we purposefully changed the state declaration and
      said <code class="inline language-javascript">first_sign_in: false</code> - because we WANT to hide
      <code class="inline language-javascript">"accept_cookies", "tos", "tour_link"</code> entities if it's not the first
      time a user signs in.

      Also note that for hiding animation we chose the speed of <code class="inline language-javascript">0</code>,
      which means all entities that need to be hidden will be hidden instantly,
      by setting <code class="inline language-css">display: none;</code>.

    %p
      Additionally <code class="inline language-javascript">DisplayStateManager</code> accepts
      <code class="inline language-javascript">pick_states_with_longest_definition_only</code> and
      <code class="inline language-javascript">debug</code> options which we'll discuss on the
      <a href="common-options.html">common options</a> page.

  = partial "shared/nav", locals: { prev: "States/StateActionManager", nxt: "States/Aliases" }
