---
title: Validations / Overview - Webface.js
---

- @page_id = "validations/overview"
.title
  %h1 Validations / Overview

.content

  %p
    While frontend validations should never be a substitute for backend validations,
    we still sometimes need them to make simple checks that would spare us additional
    requests to the server. Webface.js offers a nice set of validations and also a simple way to display
    validation errors for the form fields that triggered them.

  %p
    Suppose we have a simple comment form which only has a text field with the comment text and we want to make
    sure that this field is not null and also that the length of the comment is not less than 30 characters.
    Let's start with the html:

  %script(type="text/plain" class="language-markup line-numbers")
    :plain
      <body>

        <div data-component-class="CommentFormComponent">

          <div data-component-class="FormFieldComponent" data-component-roles="content">
            <!-- This is the element with which the value property is now associated -->
            <textarea data-component-part="value_holder"></textarea>
            <!-- This is where our errors will be displayed -->
            <div style="display: none;" data-component-attr="validation_errors_summary"></div>
          </div>

          <button data-component-class="ButtonComponent" data-component-roles="submit">
            Submit Comment
          </button>
        </div>

      </body>

  %p
    The first thing to notice here is that <code class="inline language-javascript">FormFieldComponent</code>
    is more than just a textarea, but is actually a div, which wraps
    <code class="inline language-html">&lt;textarea&gt;</code> and another
    <code class="inline language-html">&lt;div&gt;</code> where our validation errors will show up.
    This is intentional, since in our app a text field is also responsible for displaying its own validation errors
    and it's only logical to combine this all into one component.

  %p
    <code class="inline language-html">data-component-part="value_holder"</code> attribute
    on the <code class="inline language-html">&lt;textarea&gt;</code> element means that this dom element's value
    (or <code class="inline language-javascript">.val</code> property) is now associated with
    the <code class="inline language-javascript">.value</code> attribute of the component and whenever there are
    changes on either side, it gets updated on the other. We won't have to write any special code to make this work,
    <code class="inline language-javascript">FormFieldComponent</code> already contains that functionality.

  %p.note
    #{link_to "webface_rails", "/ruby-on-rails-integration/overview.html.haml"} has a form helper
    <code class="inline language-ruby">f.textarea</code>, however Webface.js doesn't have or
    need a standard component for textareas, because you can just use
    <code class="inline language-javascript">FormFieldComponent</code> with it, just like we do in our example,
    as <code class="inline language-javascript">FormFieldComponent</code> has all the necessary functionality.

  %p
    The <code class="inline language-html">&lt;div&gt;</code> with
    <code class="inline language-html">data-component-attr="validation_errors_summary"</code> obviously connects
    the component's attribute of that name to the <code class="inline language-javascript">.innerText</code> property
    of that dom element. Again, no additional code is required - errors will just appear in it,
    you'll just need to style this div accordingly (red font color, perhaps!).

  %p
    Now, we'll write the Javascript code for the <code class="inline language-javascript">CommentFormComponent</code>
    that creates and runs validations:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class CommentFormComponent extends extend_as("CommentFormComponent").mix(FormFieldComponent).with() {
          constructor() {
            super();

            this.validations = {
              "content.value": { "isNotNull"    : true },
              "content.value": { "isLongerThan" : 5    }
            };

            this.event_handlers.add({
              event: "click",
              role:  "submit",
              handler: (self,event) => {
                if(self.validate({ deep: true })) { // "deep" means validate child components too.
                  // if validations pass - send a request to create new comment here
                  console.log("Form is valid!");
                }
              }
            });

          }
        }

  %p
    If the text field is empty or it is 5 characters long or less, a list of all validation
    errors will be displayed in the appropriate dom element below the actual
    <code class="inline language-javascript">&lt;textarea&gt;</code> field. If the validations
    pass, you will see a console message saying <code class="string">"Form is valid!"</code>

  %p
    Validations can be defined on both the component itself and on its children. In fact,
    the most likely scenario is defining validations on children, because:

    %ol
      %li
        You will most likely be using standard Webface.js components for form fields
        and you don't really want to redefine them because of a single validation you need to add.
      %li
        Each form is different. Validations on one <code class="inline language-javascript">FormFieldComponent</code>
        do not usually apply to other forms or even a field with another name which is the same
        Webface.js component. Thus, defining validations on a parent that contains those
        fields is the right way to go.

  %p
    To define validations, you assign on object to component's <code class="inline language-javascript">this.validation</code>
    property - you most likely want to do that in the constructor, but you can also do it some place else, as well as
    add or remove validations from this object dynamically. The syntax for defining validations is the following:

  %pre
    %code.language-javascript
      :erb
        "[role].[attribute_name]": { "[validation 1 name]" : [argument], "[validation 1 name]" : [argument] }

  = partial "shared/nav", locals: { prev: "Standard components/something", nxt: "Validations/Standard validations" }
