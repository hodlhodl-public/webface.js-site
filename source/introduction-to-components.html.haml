---
title: Introduction to components - Webface.js
---

- @page_id = "introduction-to-components"
.title
  %h1 Introduction to components

.content

  %p
    Components are hardly a new idea in web development. Webface implements this concept without too many surprises,
    so let's look at how it does it. One thing to mention straight away is it's not based on WebComponents.

  %p
    Before going further, it also important to note that Webface provides an implementation for a number of components
    you will find useful in your development: it has buttons, checkboxes, selects, notifications and some others.
    You won't have to write all that from scratch. You can also fully customize html/css as it is separate from javascript code.

  %h2 What is a component?

  %p

    In Webface.js a component is an object of class <code class="inline language-javascript">Component</code>
    (or one of its descendants). All other components
    must inherit from it. It has a corresponding <code class="inline language-javascript">HtmlElement</code>,
    which can be accessed through the <code class="inline language-javascript">.dom_element</code>
    property. Except for the root component, whose element is assigned to be <code class="inline
    language-html">&lt;body&gt;</code> automatically when we create a new Webface.js app, html elements for
    components are assigned by identifying those html elements using the
    <code class="inline language-html">data-component-class</code> attributes. So if our
    component class is called <code class="inline language-javascript">ButtonComponent</code>
    then its corresponding html element should have a <code class="inline
    language-html">data-component-class="ButtonComponent"</code> attribute.

  %p
    For example, the following HTML code and its corresponding Javascript code will produce three
    instances of <code class="inline language-javascript">ButtonComponent</code> class:

  %pre.line-numbers
    %code.language-javascript
      :erb
        // The very long component declaration syntax is necessary for some magic.
        // Just memorize or copy-paste it, no need to think of why it is this way.
        export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
          constructor() {}
        }
  %script(type="text/plain" class="language-markup line-numbers")
    :plain
        <body>
          <button data-component-class="MyButtonComponent">Button 1</button>
          <button data-component-class="MyButtonComponent">Button 2</button>
          <button data-component-class="MyButtonComponent">Button 3</button>
        </body>

  %p
    This code isn't very helpful though, as it does nothing. Components need to be able to do something, for example react to user input: buttons should do something when user clicks on them. To explain what a component really is it's best to just list things that it has or does, because that's what truly defines it:

    %ul
      %li
        = link_to "Attributes.", "./properties-and-attributes.html", class: "bold"
        Those are just what they appear to be: Javascript object properties. However, they can be tied to the
        corresponding dom element's own properties.
      %li
        = link_to "Children and parents", "./children-and-parents.html", class: "bold"
        \- each component might have those. They are usually assigned automatically and are implied by the structure of
        the html document the app is tied to.
      %li
        = link_to "Events", "./events.html", class: "bold"
        \- those are either caused by user who clicks the button or are generated by components themselves, such as when
        a form generates a "submit" event in response to its button "click" event. Each event might have a handler which
        are concerned with program business logic and communication with the server. Normally, event handlers just call
        other methods on that component if they need to perform complex tasks.
      %li
        = link_to "Roles", "./properties-and-attributes.html", class: "bold"
        \- are an important part of how events are handled by parent components. Based on the child component role, the
        parent knows how to handle a certain event that was generated by the child.
      %li
        = link_to "Behaviors", "./properties-and-attributes.html", class: "bold"
        \- those define how a corresponding dom element of a given component changes (for example: disappears or becomes
        disabled). Behaviors are usually invoked as a response to some event occurring: for example when user clicks a
        button, this button invokes the "disable" behavior, which makes it appear disabled and makes it impossible for
        the user to click it again by accident. Behaviors should only be concerned with presentational behavior, not
        logic or communication with the server.

    Each one of those are discussed in the following sections of this manual.

  %h2 There is no HTML!

  %p
    One important aspect to note is that you have to create your own HTML for the components. Webface.js components
    don't generate any HTML - instead Webface.js scans the DOM, finds html elements with <code class="inline
    language-html">data-component-class</code> attributes and creates instances of components based on the value
    of that attribute.

  %p
    There is one case when Webface.js Component may create a new DOM element -
    from a #{link_to "template", "/templates/introduction.html"}.
    But even then the template itself is already present in the DOM and is simply
    cloned. There is no HTML embedded into the Javascript code and there is no manual creation of html elements with
    classes such as <code class="inline language-javascript">DivElement</code>.

  %p
    For standard components that come with Webface.js this documentation will provide an example html code. It is to be
    used as a reference point. A particular Component class may expect certain html elements to be present within the
    associated dom element, so that's why those examples come in handy. However, you can most likely rearrange html
    elements at your own will - the important part is having elements with attributes such as <code class="inline
    language-html">data-component-part</code> or <code class="inline
    language-html">data-component-attr</code> with particular values present - those will be specified
    explicitly. Other than that, you're free to style and rearrange as you wish.

  %p
    A #{link_to "webface-rails", "/webface-rails.html"} ruby gem solves this problem somewhat for those
    using Ruby On Rails. It provides all the necessary html views for all the standard Webface.js components plus
    some useful helper functions to build forms with Webface.js in RoR.

  %p.note
    There is a Webface.js feature planned, that would allow components to instantly notify you via a console warning that
    a corresponding dom element doesn't have necessary html elements or attributes. Currently, you need to rely on documentation
    examples or RoR templates in the corresponding RoR webface-rails library.

  = partial "shared/nav", locals: { prev: [nil, "Installation & Usage", "/installation-and-usage"], nxt: [nil, "Component initialization", "component-initialization"]}
