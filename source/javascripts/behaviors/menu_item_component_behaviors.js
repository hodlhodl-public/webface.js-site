import extend_as          from '../webface/lib/utils/mixin.js'
import ComponentBehaviors from '../webface/lib/behaviors/component_behaviors.js'

export default class MenuItemComponentBehaviors extends extend_as("MenuItemComponentBehaviors").mix(ComponentBehaviors).with() {

  expand() {
    this.component.dom_element.classList.remove("collapsed");
    this.component.dom_element.classList.add("expanded");
  }

  collapse() {
    this.component.dom_element.classList.add("collapsed");
    this.component.dom_element.classList.remove("expanded");
  }

  toggle() {
    if(this.collapsed)
      this.expand();
    else
      this.collapse();
  }

  highlight() {
    this.dom_element.classList.add("highlighted");
  }

  dehighlight() {
    this.dom_element.classList.remove("highlighted");
  }

  makeCurrent() {
    this.dom_element.classList.add("current");
  }

  get collapsed() {
    return this.dom_element.classList.contains("collapsed");
  }

}
