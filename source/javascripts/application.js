// Loading Webface components one by one. We could load webface.js instead (in that case, we wouldn't need to define Webface variable below
// but we wanted to give more flexibility here. Remove components you don't actually intend to use. Less to load!
import Logmaster                   from './webface/lib/services/logmaster.js'
import Animator                    from './webface/lib/services/animator.js'
import RootComponent               from './webface/lib/components/root_component.js'
import ButtonComponent             from './webface/lib/components/button_component.js'
import CheckboxComponent           from './webface/lib/components/checkbox_component.js'
import FormFieldComponent          from './webface/lib/components/form_field_component.js'
import NumericFormFieldComponent   from './webface/lib/components/numeric_form_field_component.js'
import ModalWindowComponent        from './webface/lib/components/modal_window_component.js'
import DialogWindowComponent       from './webface/lib/components/dialog_window_component.js'
import RadioButtonComponent        from './webface/lib/components/radio_button_component.js'
import HintComponent               from './webface/lib/components/hint_component.js'
import SimpleNotificationComponent from './webface/lib/components/simple_notification_component.js'
import SelectComponent             from './webface/lib/components/select_component.js'
import ConfirmableButtonComponent  from './webface/lib/components/confirmable_button_component.js'
import ContextMenuComponent        from './webface/lib/components/context_menu_component.js'

import './components/sidebar_component.js'
import './behaviors/menu_item_component_behaviors.js'
import './components/menu_item_component.js'

window.webface.substitute_classes = { "Animator": Animator }

export let Webface = {
  "init": (root_el=document.querySelector("body")) => {
    window.webface["logger"] = new Logmaster({test_env: false, reporters: { "console" : "DEBUG" }, throw_errors: true })
    let root = new RootComponent();
    window.webface.root_component = root;
    root.dom_element = root_el;
    root.initChildComponents();
  }
}

Webface.init();
