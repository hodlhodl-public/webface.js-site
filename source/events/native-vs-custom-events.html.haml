---
title: Events / Native vs Custom events - Webface.js
---

- @page_id = "events/native-vs-custom-events"
.title
  %h1 Events / Native vs Custom events

.content
  %p
    In Webface.js, you actually have two types of events - native and custom.
    Native events are the ones that the browser generates, such as <code class="string">click</code>
    <code class="string">mouseover</code>, <code class="string">mouseup</code> etc. The full list
    of those can be found on the #{link_to "MDN Event reference page", "https://developer.mozilla.org/en-US/docs/Web/Events"}.
    But custom events can be generated using Webface.js methods. We've already seen an example of native events above,
    and handlers used for them in the previous section of this reference.

  %h2 Native events

  %p
    The important thing to remember is that all native events need to be
    listed in the <code class="inline language-javascript">this.native_events</code> property
    of the component class or the handlers won't work. You must set in constructor:

  %pre
    %code.language-javascript
      :erb
        export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
          constructor() {
            super();
            this.native_events = ["click"];
          }
        }

  %p
    By default, Webface.js automatically prevents
    any default browser handler from being invoked if an event is listed
    in <code class="inline language-javascript">this.native_events</code>.
    For example, if a button is a link or a submit button on a form,
    then the link would not redirect and the form wouldn't be submitted. You
    can redefine this behavior by prepending the native event name with
    <code class="string">!</code> like this:

  %pre
    %code.language-javascript
      :erb
        this.native_events = ["!click"];

  %p.note
    For <code class="inline language-javascript">ButtonComponent</code> provided by Webface.js
    you can just set <code class="inline language-javascript">prevent_native_click_event</code> attrubute
    to achieve this behavior (most likely, you'd want it set through the html attribute
    <code class="inline language-html">data-prevent-native-click-event</code>.
    Or, if you're using <code class="inline language-shell">webface_rails</code>, use
    <code class="inline language-ruby">WebfaceComponentHelper#button_link</code> and 
    <code class="inline language-ruby">WebfaceForm#_submit</code> (<code class="inline language-ruby">f.submit</code>)
    to generate buttons with this attribute set.


  %h2 Native events on component parts

  %p
    Sometimes, your components may be rather complex in terms of DOM elements that construct their visual
    representation, but it's not always reasonable to break them down into sub-components. By introducing component
    <i>parts</i>, we can still catch native events from those parts without creating additional Component classes.
    To listen to a native event on component part, just prefix event name with a part name:

  %pre
    %code.language-javascript
      :erb
        this.native_events = ["!clickable_part.click"];

  %p
    Let's suppose all forms on our website need to have two buttons "Submit" and "Cancel" - say, we're absolutely
    certain about that requirement. Perhaps we then need a single component called <code class="inline language-javascript">FormButtonComponent</code>,
    which will consist of two parts - the two button elements, each with a distinct name. First, let's look at the html code:

  %script(type="text/plain" class="language-markup line-numbers")
    :plain
      <body>
        <form data-component-class="UserFormComponent">

            <div data-component-name="FormButtonComponent">
              <button data-component-part="submit">Submit</button>
              <button data-component-part="cancel">Cancel</button>
            </div>

        </form>
      </body>

  %p
    And now let's define a handlers for the events on those parts:

  %pre.line-numbers(data-line="10")
    %code.language-javascript
      :erb
         export class FormButtonComponent extends extend_as("FormButtonComponent").mix(Component).with() {
          constructor() {
            super();

            this.native_events = ["!submit.click", "!cancel.click"];

            this.event_handlers.addForEvent("click", {
              // In this case reference to "self" has a suffix
              // which is the name of the part receiving the event.
              "self.submit" : (self,event) => self.parent.dom_element.submit(), // actually submits the form
              "self.cancel" : (self,event) => console.log("Form submission cancelled")
            }); 
          }
        }

  %p
    Note that on line 10 we did something that's actually not very good idea:
    we started trying to control the parent of the button, presumably a
    <code class="inline language-javascript">UserFormComponent</code> we wrote
    (code omitted here), by calling methods on its dom element. This isn't the best solution: child component
    should normally be dumb. A better idead would be to catch a custom <code class="string">submit</code>
    event published by the button in the parent, and let it decide what to do with it.
    That is explained below.

  %h2 Custom events

  %p
    Custom events are events published by Webface.js components, which can be handled
    by their parent components or even the publisher component himself.

  %p
    To improve the previous example with <code class="inline language-javascript">FormButtonComponent</code>
    we'll make it publish custom events in response to clicks on its
    parent and will let the parent component decide what to do about it.
    But first we need to add a role to the <code class="inline language-javascript">FormButtonComponent</code>
    html:

  %script(type="text/plain" class="language-markup line-numbers" data-line="4")
    :plain
      <body>
        <form data-component-class="UserFormComponent">

            <div data-component-name="FormButtonComponent" data-component-roles="submitter">
              <button data-component-part="submit">Submit</button>
              <button data-component-part="cancel">Cancel</button>
            </div>

        </form>
      </body>

  %p
    Next, publish custom events in native event handlers:

  %pre.line-numbers(data-line="8,9")
    %code.language-javascript
      :erb
         export class FormButtonComponent extends extend_as("FormButtonComponent").mix(Component).with() {
          constructor() {
            super();

            this.native_events = ["!submit.click", "!cancel.click"];

            this.event_handlers.addForEvent("click", {
              "self.submit" : (self,event) => self.publishEvent('submit'),
              "self.cancel" : (self,event) => self.publishEvent('cancel')
            }); 
          }
        }

  %p
    And, finally, we'll handle <code class="string">submit</code> and
    <code class="string">cancel</code> events inside its parent <code class="inline language-javascript">UserFormComponent</code>.

  %pre.line-numbers(data-line="5,6")
    %code.language-javascript
      :erb
         export class UserFormComponent extends extend_as("UserFormComponent").mix(Component).with() {
          constructor() {
            super();
            this.event_handlers.addForRole("submitter", {
              submit: (self,event) => self.dom_element.submit(),
              cancel: (self,event) => console.log("Form submission cancelled")
            }); 
          }
        }

  %p
    This code works the same as before, but it allows us to reuse the <code class="inline language-javascript">FormButtonComponent</code>
    with other types forms, which may want to react to button clicks differently (for instance some forms may prompt user
    for a password before submitting the form - as a security measure).

  %h3 Another example
    
  %p
    Suppose we have a special custom text field component we call
    <code class="inline language-javascript">LimitedTextFieldComponent</code> for publishing tweets.
    When a user types more than 140 characters (yes, we know you can now tweet more than 140!) it generates a custom event called
    <code class="string">filled_up</code>, so that our <code class="inline language-javascript">PageComponent</code>
    becomes aware of the situation and maybe disables the "Send" button so user can't actually send the tweet.
    And when user removes a character we check if the field length is less then 140 chararcters, and if it is,
    it publishes a <code class="string">freed_up</code> event, and then its parent,
    <code class="inline language-javascript">PageComponent</code>, once again, picks it up.
    The idea here is that our custom text field component doesn't know what kind of warning to show,
    it just informs the parent that it's full.

  %p
    Let's start by writing some html:

  %script(type="text/plain" class="language-markup line-numbers")
    :plain
      <body data-component-class="RootComponent">
        <div data-component-class="PageComponent">
          <textarea 
            data-component-class="LimitedTextFieldComponent"
            data-component-roles="tweet_field"
          ></textarea>
          <button data-component-class="ButtonComponent">Send tweet</button>
      </body>

  %p
    And now the <code class="inline language-javascript">LimitedTextFieldComponent</code> class:

  %pre.line-numbers(data-line="12,13,20,22")
    %code.language-javascript
      :erb

        export class LimitedTextFieldComponent extends extend_as("LimitedTextFieldComponent").mix(Component).with() {
          constructor() {

            super();
            // We want both of these native events to which we'll assign
            // the same handler, because they're triggered in different cases.
            this.native_events = ["keyup", "change"];

            this.event_handlers.addForRole("#self", {
              // We're not passing `self` and `event` as arguments here
              // because we don't need them.
              keyup:  () => this.checkIfFull(),
              change: () => this.checkIfFull()
            });
          }

          checkIfFull() {
            console.log(this.dom_element.value.length);
            if(this.dom_element.value.length > 140)
              this.publishEvent("filled_up");
            else
              this.publishEvent("freed_up");
          }

        }

  %p
    This code is not terribly optimized though. We publish the <code class="string">filled_up</code> / 
    <code class="string">freed_up</code> events on practically every keypress. To keep it simple, though,
    optimization of this example code should be out of the scope of this reference.

  %p
    We can now write the <code class="inline language-javascript">PageComponent</code> class, which decides
    what do when those events are published:

  %pre.line-numbers(data-line="6,7")
    %code.language-javascript
      :erb
        export class PageComponent extends extend_as("PageComponent").mix(Component).with() {

          constructor() {
            super();
            this.event_handlers.addForRole("tweet_field", {
              filled_up: (self,event) => self.findFirstChildByRole("submit").behave("disable"),
              freed_up:  (self,event) => self.findFirstChildByRole("submit").behave("enable")
            });
          }

        }

  %p
    On lines 6-7 we use the standard <code class="inline language-javascript">Component.findFirstChildByRole()</code>
    method and then the <code class="inline language-javascript">Component.behave()</code> method dicussed in the
    #{link_to "Behaviors", "/behaviors.html"} section. But even without knowing too much about those methods,
    it's pretty easy to see what this code accomplishes: we lock and unlock the submit button based on whether our
    field is over the character limit.

  %h2 Default #all role

  %p
    There's a special <code class="string">#all</code> role that is used for
    all events of a particular name that were published by any of the child components - but only
    if an event handler isn't defined for this said event/role pair. For example:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class UserFormComponent extends extend_as("UserFormComponent").mix(Component).with() {

          constructor() {
            super();
            this.event_handlers.addForRole("#all", {
              change: (self,event) => self.validate()
            });
          }

        }

  %p
    will cause our form to call a <code class="inline language-javascript">validate()</code>
    method whenever any of the children reports a change.

  = partial "shared/nav", locals: { prev: "Events/Event handlers", nxt: "Events/Event locks" }
