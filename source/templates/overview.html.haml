---
title: Templates / Overview - Webface.js
---

- @page_id = "templates/overview"
.title
  %h1 Templates / Overview

.content

  %p
    Templates are just dom elements, that are used when a new component is being created manually
    not by parsing the DOM, but by calling <code class="inline language-javascript">Component.create</code> 
    static method / constructor.

  %p
    Let's say users of our site can post comments. When the page
    loads, we might have some comments already on the page - for those, <code class="inline language-javascript">CommentComponent</code>
    instances will be created automatically. However, when our user adds a new comment, we'll need to create a dom element
    to be appended to the comment's list. In this case, we first create an instance of the <code class="inline language-javascript">CommentComponent</code>,
    which will search the DOM for the element with <code class="inline language-html">data-component-template="CommentComponent"</code>
    attribute, clone it and assign the newly created component's dom element to the cloned element.

  %p
    After that, we can add this newly created component to another component as a child, which will automatically append it
    to the parent's dom element.

  %p
    Let's illustrate this with some code - and for simplicity, we'll make it so that new comments always have the same text -
    there will be no form to enter text and they are added simply by clicking the button. First, the html code:

  %script(type="text/plain" class="language-markup line-numbers" data-line="3,4")
    :plain
      <body data-component-class="RootComponent">

        <div data-component-class="Component" data-component-roles="comment_container">
          <div data-component-template="CommentComponent"
               data-component-class="CommentComponent"
               data-component-attr="text">
            This is my first comment!
          </div>
        </div>

        <button data-component-class="AddCommentButtonComponent">Add comment</button>

      </body>

  %p
    Notice on line 3 we created a dom element for <code class="inline language-javascript">Component</code> -
    why is that? That's because this will be our container for comments (as indicated by the assigned role). When creating
    new components from a template, we can specify a parent component and if we don't do that, components
    will be added as children to <code class="inline language-javascript">RootComponent</code>, which isn't desirable in our case.
    But we still need a container and <code class="inline language-javascript">Component</code> is perfect for it
    because we don't need any additional functionality.

  %p.note
    This need for an actual component to be the container is probably a bit of an overcomplication. As of right now
    that's the only way to do things, but Webface.js will be adding the option to just specify an arbitrary dom element as a
    container, while implying the parent component.

  %p
    Also notice that the element for the template is also automatically the first comment. While acceptable, it's
    not always desirable. If, for example, we were to delete all comments from the page, we would have no template,
    and trying to add a new comment would result in an error. Thus, perhaps it's better to make the template a separate
    element and to make it invisible:

  %script(type="text/plain" class="language-markup line-numbers" data-line="14-16")
    :plain
      <body data-component-class="RootComponent">

        <div data-component-class="Component" data-component-roles="comment_container">
          <div data-component-template="CommentComponent" data-component-attr="text">
            This is my first comment!
          </div>
        </div>

        <button data-component-class="AddCommentButtonComponent">Add comment</button>

        <!-- if you have many component templates, it makes sense to
             put them all inside one hidden block, instead of making
             each one of them invisible -->
        <div style="display: none;">
          <div data-component-template="CommentComponent" data-component-attr="text"></div>
        </div>

      </body>

  %p
    And we are now ready to write the Javascript code that will actually create new comments when the button is
    clicked. We first will write the code for the <code class="inline language-javascript">CommentComponent</code> class.
    It's going to be very short as we only need to specify the attribute in which comment text will be stored
    the attribute will be called <code class="string">text</code> (who would've thought!).

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class CommentComponent extends extend_as("CommentComponent").mix(Component).with() {
          constructor() {
            super();
            this.attribute_names = ["text"];
          }
        }

  %p
    The button code will look a bit more complex as we need to do a bunch of things there:

  %pre.line-numbers(data-line="10,15")
    %code.language-javascript
      :erb
        export class AddCommentButtonComponent extends extend_as("AddCommentButtonComponent").mix(Component).with() {

          constructor() {
            super();
            this.native_events = ["click"];
            this.event_handlers.add({
              event: "click",
              role: "#self",
              handler: (self,event) => {
                CommentComponent.create({container: self.comment_container, attrs: { text: "New comment" }});
              }
            });
          
          afterInitialize() {
            this.comment_container = RootComponent.instance.findFirstChildByRole("comment_container");
          }

        }

  %p
    Don't get confused, line 15 will run before line 10! In line 14, we just decide what component instance
    is going to be the container for our comments. And the we just pass it to the
    <code class="inline language-javascript">CommentComponet.create()</code> in an argument named
    <code class="inline language-javascript">container</code>.

  %p
    The static method <code class="inline language-javascript">Component.create</code> (and its alias,
    <code class="inline language-javascript">Component.createFromTemplate()</code>) is available in all
    components that inherit from <code class="inline language-javascript">Component</code> and that's
    the method that does the heavy lifting of initializing everything, adding dom element and adding
    the newly created component to the right parent.


  %p
    <code class="inline language-javascript">Component.create()</code> accepts <code class="inline language-javascript">attrs</code>
    argument and by passing an object with attribute names as keys you can that way set values
    for attributes in a newly created component.


  %p
    When we run this code and click the button, our html is updated to look like this:

  %script(type="text/plain" class="language-markup line-numbers" data-line="8-10")
    :plain
       <body data-component-class="RootComponent">
        <div data-component-class="Component" data-component-roles="comment_container">

          <div data-component-class="CommentComponent" data-component-attr="text">
            This is my first comment!
          </div>

          <div data-component-class="CommentComponent" data-component-attr="text">
            New comment
          </div>
        </div>
      ...
      </body>


  %p
    The power of templates is that they are non-obligatory. You may have a fully functional app written without any templates.
    It is only when you need to dynamically add new components onto the page you need templates. Even then, in some cases,
    you might decide to use an existing visible element as a template (as shown in the first version of the html for this usecase),
    since this simplifies the page itself.

  = partial "shared/nav", locals: { prev: "Behaviors", nxt: "Templates/Named templates" }
