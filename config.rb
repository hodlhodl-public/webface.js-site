# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page '/stylesheets/*.css', layout: false

set :haml, { :format => :html5 }
activate :livereload

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end

helpers do
  def menu_item(title, custom_link=nil, &block)
    @current_menu_path ||= []
    link = snakecase(title.gsub("&", "and"), "-")
    if block
      page_id = nil
      @current_menu_path << link
      block_content = capture_html(&block)
      @current_menu_path.pop
    else
      link = "/" + (@current_menu_path + [link]).join("/") + ".html"
      page_id = link.sub(/\A\//, "").sub(".html", "")
      block_content = nil
    end
    partial "shared/menu_item", locals: {
      title: title,
      link: custom_link || link,
      type: block_given? ? "parent" : nil,
      page_id: page_id,
      block_content: block_content
    }
  end

  def expand_nav_from_string(s)
    path = s.split("/")
    page_title = path.last
    section = path[-2] ? path[-2] : nil
    [section, page_title, "/#{snakecase(s).downcase.gsub("&", "and").gsub(/(\s|_)/, "-")}.html"]
  end

  def snakecase(s, separator="_")
    s.gsub(/([A-Z]+)([A-Z][a-z])/,'\1' + separator + '\2').
    gsub(/([a-z\d])([A-Z])/,'\1' + separator + '\2').
    tr('-', separator).
    gsub(/\s/, separator).
    gsub(/__+/, separator).
    downcase
  end

end
